#ifndef VGOS_PIC_H_

void pic_init(void);
void pic_enable(u8 n);
void pic_disable(u8 n);
void pic_mask(u16 mask);

#define VGOS_PIC_H_
#endif
