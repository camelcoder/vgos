#ifndef VGOS_CR_H_
#include <vgos/types.h>

#define CR0_PROTECTED_MODE_ENABLE (1u << 0u)
#define CR0_MONITOR_CO_PROCESSOR  (1u << 1u)
#define CR0_EMULATION             (1u << 2u)
#define CR0_TASK_SWITCHED         (1u << 3u)
#define CR0_EXTENSION_TYPE        (1u << 4u)
#define CR0_NUMERIC_ERROR         (1u << 5u)
#define CR0_WRITE_PROTECT         (1u << 16u)
#define CR0_ALIGNMENT_CHECK       (1u << 18u)
#define CR0_NOT_WRITE_THROUGH     (1u << 29u)
#define CR0_CACHE_DISABLE         (1u << 30u)
#define CR0_PAGING                (1u << 31u)

#define CR3_PCID_MASK 0xfffu
#define CR3_PML4_MASK 0xfffffffffffff000u

#define CR4_V8086_MODE_EXTENSIONS                       (1u << 0u)
#define CR4_PROTECTED_MODE_VIRTUAL_INTERRUPTS           (1u << 1u)
#define CR4_TIME_STAMP_RESTRICT_RING_0                  (1u << 2u)
#define CR4_DEBUGGING_EXTENSIONS                        (1u << 3u)
#define CR4_PAGE_SIZE_EXTENSIONS                        (1u << 4u)
#define CR4_PHYSICAL_ADDRESS_EXTENSION                  (1u << 5u)
#define CR4_MACHINE_CHECK_EXCEPTION                     (1u << 6u)
#define CR4_PAGE_GLOBAL_ENABLED                         (1u << 7u)
#define CR4_PERFORMANCE_MONITORING_COUNTER_ENABLE       (1u << 8u)
#define CR4_OS_SUPPORT_FXSAVE_FXSTORE                   (1u << 9u)
#define CR4_OS_SUPPORT_UNMASKED_SIMD_FP_EXCEPTIONS      (1u << 10u)
#define CR4_USER_MODE_INSTRUCTION_PREVENTION            (1u << 11u)
#define CR4_VIRTUAL_MACHINE_EXTENSIONS_ENABLED          (1u << 13u)
#define CR4_SAFER_MODE_EXTENSIONS_ENABLED               (1u << 14u)
#define CR4_PCID_ENABLED                                (1u << 17u)
#define CR4_XSAVE_PROCESSOR_EXTENDED_STATES_ENABLED     (1u << 18u)
#define CR4_SUPERVISOR_MODE_EXECUTION_PREVENTION_ENABLE (1u << 20u)
#define CR4_SUPERVISOR_MODE_ACCESS_PROTECTION_ENABLE    (1u << 21u)

extern u64 cr0_get();
extern u64 cr2_get();
extern u64 cr3_get();
extern u64 cr4_get();
extern void cr0_set(u64 val);
extern void cr2_set(u64 val);
extern void cr3_set(u64 val);
extern void cr4_set(u64 val);

#define VGOS_CR_H_
#endif
