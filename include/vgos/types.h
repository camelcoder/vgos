#ifndef VGOS_TYPES_H_

typedef signed char  schar;
typedef signed short sshort;
typedef signed int   sint;
typedef signed long  slong;

typedef unsigned char  uchar;
typedef unsigned short ushort;
typedef unsigned int   uint;
typedef unsigned long  ulong;

typedef schar  s8;
typedef sshort s16;
typedef sint   s32;
typedef slong  s64;
typedef s64 ssize_t;

typedef uchar  u8;
typedef ushort u16;
typedef uint   u32;
typedef ulong  u64;
typedef u64 size_t;

typedef float  f32;
typedef double f64;

#define VGOS_TYPES_H_
#endif
