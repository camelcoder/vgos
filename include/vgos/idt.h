#ifndef VGOS_IDT_H_
#include <vgos/types.h>

void idt_init(void);
void idt_start(void);
void idt_close(void);
void idt_register(u8 index, void (*callback)(void));

#define VGOS_IDT_H_
#endif
