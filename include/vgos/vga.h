#ifndef VGOS_VGA_H_
#include <vgos/types.h>

enum VgaMode {
	VGA_TXT_FIRST,
	VGA_TXT_40_25 = VGA_TXT_FIRST,
	VGA_TXT_40_50,
	VGA_TXT_80_25,
	VGA_TXT_80_50,
	VGA_TXT_90_30,
	VGA_TXT_90_60,
	VGA_TXT_LAST = VGA_TXT_90_60,

	VGA_GFX_FIRST,
	VGA_GFX_640_480_2 = VGA_GFX_FIRST,
	VGA_GFX_320_200_4,
	VGA_GFX_640_480_16,
	VGA_GFX_720_480_16,
	VGA_GFX_320_200_256,
	VGA_GFX_320_200_256X,
	VGA_GFX_LAST = VGA_GFX_320_200_256X,
	VGA_MODE_COUNT
};

struct VgaModeInfo {
	int width, height;
	int depth;
};

struct VgaModeInfo vga_set_mode(enum VgaMode mode);

#define VGOS_VGA_H_
#endif
