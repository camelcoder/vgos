#ifndef VOGS_MULTIBOOTINFO_H_

#include <vgos/types.h>

#define MULTIBOOT_FB_TYPE_INDEXED  0
#define MULTIBOOT_FB_TYPE_RGB      1
#define MULTIBOOT_FB_TYPE_EGA_TEXT 2

struct MultibootInfo {
	/* Multiboot info version number */
	u32 flags;

	/* Available memory from BIOS */
	u32 memLower;
	u32 memUpper;

	/* "root" partition */
	u32 bootDevice;

	/* Kernel command line */
	u32 cmdline;

	/* Boot-Module list */
	u32 modsCount;
	u32 modsAddr;

	union {
		struct {
			u32 tabsize;
			u32 strsize;
			u32 addr;
			u32 reserved;
		} aoutSym;
		struct {
			u32 num;
			u32 size;
			u32 addr;
			u32 shndx;
		} elfSec;
	} u;

	/* Memory Mapping buffer */
	u32 mmapLength;
	u32 mmapAddr;

	/* Drive Info buffer */
	u32 drivesLength;
	u32 drivesAddr;

	/* ROM configuration table */
	u32 configTable;

	/* Boot Loader Name */
	u32 bootLoaderName;

	/* APM table */
	u32 apmTable;

	/* Video */
	u32 vbeControlInfo;
	u32 vbeModeInfo;
	u16 vbeMode;
	u16 vbeInterfaceSeg;
	u16 vbeInterfaceOff;
	u16 vbeInterfaceLen;

	u64 fbAddr;
	u32 fbPitch;
	u32 fbWidth;
	u32 fbHeight;
	u8 fbBpp;
	u8 fbType;
	union {
		struct {
			u32 addr;
			u16 numColors;
		} palette;
		struct {
			u8 red_field_position;
			u8 red_mask_size;
			u8 green_field_position;
			u8 green_mask_size;
			u8 blue_field_position;
			u8 blue_mask_size;
		} color;
	} fb;
};

extern struct MultibootInfo *multibootInfo;

#define VOGS_MULTIBOOTINFO_H_
#endif
