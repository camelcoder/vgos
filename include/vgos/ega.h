#ifndef VGOS_EGA_H_
#include <vgos/types.h>

#define ega_mvget(x, y)       (ega_move((x), (y)), ega_get())
#define ega_mvputc(x, y, chr) (ega_move(x, y), ega_putc(chr))
#define ega_mvputs(x, y, str) (ega_move(x, y), ega_puts(str))

enum EgaColor {
	EGA_FG_BLACK      = 0x0 << 8,
	EGA_FG_BLUE       = 0x1 << 8,
	EGA_FG_GREEN      = 0x2 << 8,
	EGA_FG_CYAN       = 0x3 << 8,
	EGA_FG_RED        = 0x4 << 8,
	EGA_FG_MAGENTA    = 0x5 << 8,
	EGA_FG_BROWN      = 0x6 << 8,
	EGA_FG_GRAY_LIGHT = 0x7 << 8,

	EGA_FG_GRAY_DARK   = 0x8 << 8,
	EGA_FG_BLUE_LIGHT  = 0x9 << 8,
	EGA_FG_GREEN_LIGHT = 0xa << 8,
	EGA_FG_CYAN_LIGHT  = 0xb << 8,
	EGA_FG_RED_LIGHT   = 0xc << 8,
	EGA_FG_PINK        = 0xd << 8,
	EGA_FG_YELLOW      = 0xe << 8,
	EGA_FG_WHITE       = 0xf << 8,

	EGA_BG_BLACK      = 0x0 << 12,
	EGA_BG_BLUE       = 0x1 << 12,
	EGA_BG_GREEN      = 0x2 << 12,
	EGA_BG_CYAN       = 0x3 << 12,
	EGA_BG_RED        = 0x4 << 12,
	EGA_BG_MAGENTA    = 0x5 << 12,
	EGA_BG_BROWN      = 0x6 << 12,
	EGA_BG_GRAY_LIGHT = 0x7 << 12,

	EGA_BLINK = 1 << 15
};

int ega_is_active(void);
int ega_rows(void);
int ega_cols(void);
u16 *ega_addr(void);

void ega_clear(void);
void ega_fill(int chr);

void ega_set_color(int col);
void ega_move(int x, int y);
int ega_get_x(void);
int ega_get_y(void);

int ega_get(void);
void ega_nextline(void);
void ega_putc(int chr);
void ega_puts(char *str);

#define VGOS_EGA_H_
#endif
