BITS 32

extern textmode_print

global check_cpuid
global check_long_mode

section .text

check_cpuid:
	; flip ID bit in EFLAGS
	pushfd
	pop eax ; copy EFLAGS
	mov ecx, eax ; store old EFLAGS
	xor eax, 1 << 21 ; flip 21st bit, ID bit
	push eax
	popfd ; load EFLAGS (with ID bit inverted)

	; retrieve EFLAGS
	pushfd
	pop eax
	push ecx
	popfd ; load old EFLAGS

	; check if the flag is set
	cmp eax, ecx
	je .no_cpuid
	ret
.no_cpuid:
	mov esi, .nocpuidMsg
	call textmode_print
.halt:
	cli
	hlt
	jmp .halt

.nocpuidMsg db "CPUID not supported, system halted", 0


check_long_mode:
	; check for extended cpuid (> 0x80000000)
	mov eax, 0x80000000
	cpuid
	cmp eax, 0x80000001
	jb .no_long_mode ; if result is bellow 0x80000001

	; check for long mode
	mov eax, 0x80000001
	cpuid
	test edx, 1 << 29
	jz .no_long_mode ; if 29st bit, LM-bit, isn't set
	ret
.no_long_mode:
	mov esi, .nolongmodeMsg
	call textmode_print
.halt:
	cli
	hlt
	jmp .halt

.nolongmodeMsg db "Long mode not available, system halted", 0

