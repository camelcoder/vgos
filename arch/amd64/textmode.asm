BITS 32

global textmode_print
global textmode_clear
global textmode_disable_cursor

section .text

textmode_print:
	pusha
	mov edi, 0xb8000
.loop:
	lodsb
	test al, al
	jz .out
	stosb
	inc edi
	jmp .loop
.out:
	popa
	ret

textmode_disable_cursor:
	pushf
	push eax
	push edx

	mov dx, 0x3D4
	mov al, 0xA ; low cursor shape register
	out dx, al

	inc dx
	mov al, 0x20 ; 6-7 unused, 5 disables cursor, 0-4 cursor shape
	out dx, al

	pop edx
	pop eax
	popf
	ret

textmode_clear:
	pusha
	mov edi, 0xb8000
	mov ecx, 80*25
	mov al, ' '
	mov ah, 0x07
	rep stosw
	popa
	ret
