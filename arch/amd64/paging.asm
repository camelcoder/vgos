BITS 32

global paging_init

section .text
paging_init:
	mov ecx, 0
.loop:
	mov eax, (1 << 21) ; 2M base
	mul ecx
	or eax, 0x83 ; add attrs
	mov [kernel_cr3.pdt_base + ecx * 8], eax
	inc ecx
	cmp ecx, 512
	jne .loop

	; map 0xfd00_0000 + 24M (12 x 2M-page) area for framebuffer usage
	mov ecx, 3
	mov eax, kernel_cr3.pdt_framebuffer + 0x3
	mov [kernel_cr3.pdpt_base + ecx * 8], eax

	mov ecx, 0
.loop2:
	mov eax, (1 << 21)
	mul ecx
	add eax, 0xfd000000
	or eax, 0x83
	mov [kernel_cr3.pdt_base + ecx * 8 + 3904], eax
	inc ecx
	cmp ecx, 12
	jne .loop2

	; enable paging
	mov eax, kernel_cr3.pml4t_base
	mov cr3, eax

	; enable PAE(physical address extension)
	mov eax, cr4
	or eax, 1 << 5
	mov cr4, eax

	; enable long mode
	mov ecx, 0xC0000080 ; EFER MSR
	rdmsr
	or eax, 1 << 8 ; LM-bit
	wrmsr

	; enable paging
	mov eax, cr0
	or eax, 1 << 31 ; PG-bit
	mov cr0, eax
	ret

section .data
align 4096
kernel_cr3:
.pml4t_base: ; page-map level-4 entry
	dq .pdpt_base + 0x3 ; S,R/W,P
	times (0x200 - 2) dq 0
	dq .pml4t_base + 0x3 ; recursive-mapping 511-th entry
.pdpt_base: ; page-directory-pointer entry
	dq .pdt_base + 0x3 ; S,R/W,P
	times (0x200 - 1) dq 0
.pdt_base: ; page-directory entry
	times 0x200 dq 0
.pdt_framebuffer:
	times 0x200 dq 0
.end:












;bits 32
;
;global paging_init
;global paging_enable
;
;PAGE_TABLES equ 16 ; 16 page tables == 32 MiB mapped
;
;section .bss
;align 4096
;kernel_cr3:
;.pml4:
;	resb 4096
;.pdpt_low:
;	resb 4096
;.pdpt_hi:
;	resb 4096
;.pdt:
;	resb 4096
;.pt:
;	resb 4096 * PAGE_TABLES
;.end:
;
;section .text
;paging_init:
;	; clear page tables
;	xor eax, eax
;	mov edi, kernel_cr3;
;	mov ecx, (kernel_cr3.end - kernel_cr3) / 4
;	rep stosd
;
;	; set up page tables
;	mov eax, 0x03 ; rw bits
;	mov edi, kernel_cr3.pt
;	mov ecx, 512 * PAGE_TABLES
;.loop0:
;	stosd
;	push eax
;	xor eax, eax
;	stosd
;	pop eax
;	add eax, 4096
;	loop .loop0
;
;	; set up page directories
;	mov eax, kernel_cr3.pt
;	or eax, 0x03 ; rw bits
;	mov edi, kernel_cr3.pdt
;	mov ecx, PAGE_TABLES
;.loop1:
;	stosd
;	push eax
;	xor eax, eax
;	stosd
;	pop eax
;	add eax, 4096
;	loop .loop1
;
;	; set up pdpt
;	mov eax, kernel_cr3.pdt
;	or eax, 0x03 ; rw bits
;	mov edi, kernel_cr3.pdpt_low
;	stosd
;	xor eax, eax
;	stosd
;
;	mov eax, kernel_cr3.pdt
;	or eax, 0x03 ; rw bits
;	mov edi, kernel_cr3.pdpt_hi + 511*8
;	stosd
;	xor eax, eax
;	stosd
;
;	; set up pml4
;	mov eax, kernel_cr3.pdpt_low
;	or eax, 0x03 ; rw bits
;	mov edi, kernel_cr3.pml4
;	stosd
;	xor eax, eax
;	stosd
;
;	mov eax, kernel_cr3.pdpt_low
;	or eax, 0x03 ; rw bits
;	mov edi, kernel_cr3.pml4 + 256*8
;	stosd
;	xor eax, eax
;	stosd
;
;	mov eax, kernel_cr3.pdpt_hi
;	or eax, 0x03 ; rw bits
;	mov edi, kernel_cr3.pml4 + 511*8
;	stosd
;	xor eax, eax
;	stosd
;
;paging_enable:
;	mov eax, kernel_cr3
;	mov cr3, eax
;
;	; enable PAE(physical address extension) paging
;	mov eax, cr4
;	or eax, 1 << 5
;	mov cr4, eax
;
;	; enable long mode
;	mov ecx, 0xC0000080
;	rdmsr
;	or eax, 1 << 8
;	wrmsr
;
;	; enable paging
;	mov eax, cr0
;	or eax, 1 << 31
;	mov cr0, eax
;	ret
