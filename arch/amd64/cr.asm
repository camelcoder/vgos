BITS 64

global cr0_get
global cr2_get
global cr3_get
global cr4_get
global cr0_set
global cr2_set
global cr3_set
global cr4_set

section .text

%assign i 0
%rep 4
	cr%+ i%+ _get:
		mov rax, cr%+ i
		ret

	cr%+ i%+ _set:
		mov cr%+ i, rdi
		ret
%assign i i+1
%endrep
