BITS 64

extern gdt64.code
extern pic_init

global idt_init
global idt_start
global idt_close
global idt_callbacks

IDT_NUM_ENTRIES equ 256
IRQ_OFFSET equ 0x20

PORT_PIC_MC equ 0x20
PORT_PIC_MD equ 0x21
PORT_PIC_SC equ 0xa0
PORT_PIC_SD equ 0xa1


section .text
idt_init:
	%assign i 0
	%rep IDT_NUM_ENTRIES
		mov rax, isr_%+ i
		mov WORD [idtStart + i * 16], ax ; low
		shr rax, 16
		mov WORD [idtStart + i * 16 + 6], ax ; mid
		shr rax, 16
		mov DWORD [idtStart + i * 16 + 8], eax ; high
	%assign i i+1
	%endrep

	lidt [idtInfo]
	ret

idt_start:
	sti
	ret


idt_close:
	cli
	ret



inter_ignore:
	ret


%assign i 0
%rep IDT_NUM_ENTRIES
isr_%+ i:
	; Pop error code if existing
	%if i == 8 || (i >= 10 && i <= 15)
		add rsp, 1
	%endif

	push rax
	push rbx
	push rcx
	push rdx
	push rsi
	push rdi
	push rbp
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15

	call [idt_callbacks + 8 * i]

	; Send master EndOfInterrupt signal
	mov ax, PORT_PIC_MC
	out PORT_PIC_MC, ax

	; Send slave EndOfInterrupt signal
	%if IRQ_OFFSET + 8 <= i
		mov ax, PORT_PIC_MC
		out PORT_PIC_SC, ax
	%endif

	pop r15
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop rbp
	pop rdi
	pop rsi
	pop rdx
	pop rcx
	pop rbx
	pop rax
	iretq

%assign i i+1
%endrep


section .data
align 4
idtStart:
;===================
; Idt Entry layout:
;===================
; offsetLow : 16  ; irsLow
; selector : 16   ; gdt64.code
; ist : 5         ; interrupt stack tableoffset
; zero : 3        ;
; type : 4        ; 0b1110 -> 80386 32-bit interrupt gate
; zerobit : 1
; ring : 2        ; 0b00 -> ring 0
; present : 1     ; 0b1 -> used interrupt
; offsetMid : 16  ; irsMid
; offsetHigh : 32 ; irsHigh
; zero : 32       ; reserve
;===================
times IDT_NUM_ENTRIES dw 0, gdt64.code, 0x8e00, 0, 0, 0, 0, 0
;===================
idtEnd:

idtInfo:
	dw idtEnd - idtStart - 1
	dq idtStart

idt_callbacks:
	times IDT_NUM_ENTRIES dq inter_ignore
