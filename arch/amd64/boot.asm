BITS 32

extern textmode_clear
extern textmode_disable_cursor
extern check_cpuid
extern check_long_mode
extern paging_init
extern gdt64.desc
extern gdt64.code
extern kmain

global vgos_start
global multibootInfo

MAGIC      equ 0x1BADB002
PAGE_ALIGN equ (1 << 0)
MEMINFO    equ (1 << 1)
VIDEO_MODE equ (1 << 2)
FLAGS      equ (PAGE_ALIGN | VIDEO_MODE | MEMINFO)

section .multiboot
align 4
dd MAGIC
dd FLAGS
dd -(MAGIC + FLAGS)
times 5 dd 0 ; ignore AOUT_KLUDGE settings
dd 1 ; 0 -> lear graphics mode | 1 -> EGA
dd 0 ; width
dd 0 ; height
dd 0 ; depth

section .text
vgos_start:
	push eax
	mov eax, multibootInfo
	mov [eax], ebx
	pop eax

	mov esp, vgos_stack

	call textmode_disable_cursor
	call textmode_clear
	call check_cpuid
	call check_long_mode

	call paging_init

	lgdt [gdt64.desc]
	jmp gdt64.code:.long_start
	bits 64
.long_start:
	mov ax, 0 ; load 0 into all data segment registers
	mov ss, ax
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	mov rsp, vgos_stack

	call kmain

vgos_exit:
	cli
	hlt
	jmp vgos_exit

section .data
multibootInfo dq 0


section .bss
align 16
resb 1024 * 1024 * 2
vgos_stack:

