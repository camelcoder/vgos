#include <vgos/idt.h>

extern void (*idt_callbacks[256])(void);

void idt_register(u8 index, void (*callback)(void))
{
    idt_callbacks[index] = callback;
}
