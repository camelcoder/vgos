AS := nasm -f elf64
LD := ld -melf_x86_64 -nostdlib
CC := c99 -march=x86-64 -m64 -mabi=sysv -ffreestanding -nostdlib -pipe -fno-stack-protector
