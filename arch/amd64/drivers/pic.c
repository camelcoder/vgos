#include <vgos/types.h>
#include <vgos/ports.h>


#define IRQ_OFFSET 0x20
#define ICW_MASK (0x10 | 0x01) /* ICW1_INIT | ICW1_ICW4 */

void pic_init(void)
{
	/* Start the initialization sequence (in cascade mode) */
	outb(0x20, ICW_MASK); io_wait();
	outb(0xa0, ICW_MASK); io_wait();

	/* Set command offsets */
	outb(0x21, IRQ_OFFSET); io_wait();
	outb(0xa1, IRQ_OFFSET + 0x08); io_wait();

	/* Tell Master PIC that there is a slave PIC at IRQ2 (0000 0100) */
	outb(0x21, 0x04); io_wait();
	/* Tell Slave PIC its cascade identity (0000 0010) */
	outb(0xa1, 0x02); io_wait();

	/* Enable 8086/88 (MCS-80/85) mode */
	outb(0x21, 0x01); io_wait();
	outb(0xa1, 0x01); io_wait();

	/* Clear InterruptMaskRegister (enable all interrupts) */
	outb(0x21, 0xFF); io_wait();
	outb(0xa1, 0xFF); io_wait();
}

void pic_enable(u8 n)
{
	if (n < 8) {
		outb(0x21, inb(0x21) & ~((u8)1 << n));
	} else {
		outb(0xa1, inb(0xa1) & ~((u8)1 << n));
	}
	io_wait();
}
void pic_disable(u8 n)
{
	if (n < 8) {
		outb(0x21, inb(0x21) | ((u8)1 << n));
	} else {
		outb(0xa1, inb(0xa1) | ((u8)1 << n));
	}
	io_wait();
}
void pic_mask(u16 mask)
{
	outb(0x21, mask & 0xFF); io_wait();
	outb(0xa1, (mask >> 16) & 0xFF ); io_wait();
}

