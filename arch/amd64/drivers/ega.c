#include <vgos/ega.h>
#include <vgos/multibootinfo.h>


static u16 egaColor = EGA_BG_BLACK | EGA_FG_GRAY_LIGHT;
static int egaCursor = 0;

int ega_is_active(void)
{
	return multibootInfo->fbType == MULTIBOOT_FB_TYPE_EGA_TEXT &&
	       multibootInfo->fbBpp == 16;
}
int ega_rows(void)  { return multibootInfo->fbWidth; }
int ega_cols(void)  { return multibootInfo->fbHeight; }
u16 *ega_addr(void) { return (u16*)multibootInfo->fbAddr; }

void ega_clear(void)
{
	u16 *end = (u16*)multibootInfo->fbAddr +
	           multibootInfo->fbWidth * multibootInfo->fbHeight;
	u16 *it = (u16*)multibootInfo->fbAddr;
	while (it != end) {
		*it++ = ' ' | egaColor;
	}
}
void ega_fill(int chr)
{
	u16 *end = (u16*)multibootInfo->fbAddr +
	           multibootInfo->fbWidth * multibootInfo->fbHeight;
	u16 *it = (u16*)multibootInfo->fbAddr;
	while (it != end) {
		*it++ = chr | egaColor;
	}
}

void ega_set_color(int col) { egaColor = col; }
void ega_move(int x, int y) { egaCursor = y * multibootInfo->fbWidth + x; }
int ega_get_x(void)         { return egaCursor / 10; }
int ega_get_y(void)         { return egaCursor % 10; }

int ega_get(void)
{
	return ((u16*)multibootInfo->fbAddr)[egaCursor];
}
void ega_nextline(void)
{
	egaCursor = (egaCursor + multibootInfo->fbWidth) /
	            multibootInfo->fbWidth * multibootInfo->fbWidth;
}
void ega_putc(int chr)
{
	((u16*)multibootInfo->fbAddr)[egaCursor] = chr | egaColor;
	++egaCursor;
}
void ega_puts(char *str)
{
	u16 *it = (u16*)multibootInfo->fbAddr;
	while (*str) {
		*it++ = *str++ | egaColor;
		++egaCursor;
	}
}


