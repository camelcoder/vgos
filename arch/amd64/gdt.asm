BITS 64

global gdt64.desc
global gdt64.code

section .rodata
align 16
gdt64:
.zero: equ $ - gdt64
	dq 0
.code: equ $ - gdt64
	; 0-41  ignored         ignored in 64-bit mode
	; 42    conforming      enable for higher privilege level
	; 43    executable      if set, it's a code segment, else it's a data segment
	; 44    descriptor type 1 for code and data segments
	; 45-46 privilege       ring level
	; 47    present         1 for valid selectors
	; 48-52 ignored         ignored in 64-bit mode
	; 53    64-bit          1 for 64-bit code segments
	; 54    32-bit          0 for 64-bit segments
	; 55-63 ignored         ignored in 64-bit mode
	dq (1<<43) | (1<<44) | (1<<47) | (1<<53) ; code segment
.desc:
	dw $ - gdt64 - 1
	dq gdt64
