BITS 64

global io_wait
global outb
global outw
global outl
global inb
global inw
global inl

section .text
io_wait:
	jmp .1
	.1:
	jmp .2
	.2:
	ret

outb:
	mov rdx, rdi
	mov rax, rsi
	out dx, al
	ret
outw:
	mov rdx, rdi
	mov rax, rsi
	out dx, ax
	ret
outl:
	mov rdx, rdi
	mov rax, rsi
	out dx, eax
	ret

inb:
	mov rdx, rdi
	in al, dx
	ret
inw:
	mov rdx, rdi
	in ax, dx
	ret
inl:
	mov rdx, rdi
	in eax, dx
	ret
