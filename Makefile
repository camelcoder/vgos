ARCH_DIR := arch/amd64/

include $(ARCH_DIR)/config.mk

CFLAGS := -Iinclude/ \
          -Wall -Wextra -pedantic -Wshadow \
          -Wdouble-promotion -Wno-return-void -Wundef \

SRC  := $(shell find $(ARCH_DIR) drivers init kernel lib -type f -name "*.c" -or -name "*.asm")
OBJ  := $(SRC:%.c=%-c.o)
OBJ  := $(OBJ:%.asm=%-asm.o)

VGOS_BUILD_DIR := build/

VGOS_OBJ := $(VGOS_BUILD_DIR)vgos.o
VGOS_CFG := $(VGOS_BUILD_DIR)config.mk
VGOS_LNK := $(VGOS_BUILD_DIR)linker.ld

all: $(VGOS_OBJ) $(VGOS_CFG)


%-c.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $< -g

%-asm.o: %.asm
	$(AS) -o $@ $< -g


$(VGOS_BUILD_DIR):
	mkdir $@

$(VGOS_OBJ): $(OBJ) $(VGOS_LNK) $(VGOS_BUILD_DIR)
	ld -T $(VGOS_LNK) -r $(OBJ) -o $@

$(VGOS_LNK): $(ARCH_DIR)/linker.ld $(VGOS_BUILD_DIR)
	cp $< $@

$(VGOS_CFG): $(ARCH_DIR)/config.mk $(VGOS_BUILD_DIR)
	cp $< $@


options:
	@echo vgos build options:
	@echo "CC       = $(CC)"
	@echo "AS       = $(AS)"
	@echo "LD       = $(LD)"
	@echo "CFLAGS   = $(CFLAGS)"
	@echo "ARCH_DIR = $(ARCH_DIR)"

clean:
	rm -rf $(OBJ) $(VGOS_BUILD_DIR)

.PHONY: all options clean
