#include <vgos/types.h>
#include <vgos/idt.h>
#include <vgos/pic.h>
#include <vgos/multibootinfo.h>
#include <vgos/ega.h>


void print_raw(u16 *base, void const* str_)
{
	u8 const *str = str_;
	for (; *str; ++str, ++base)
		*base = (*base & 0xff00) | *str;
}

void print(void const* str_)
{
	u8 const *str = str_;

	for (; *str; ++str) {
		switch (*str)
		{
		case '\n':
			ega_nextline();
			break;
		default:
			ega_putc(*str);
			break;
		}
	}
}

void println(void const *str)
{
	print(str);
	print("\n");
}
void print_byte(u8 key)
{
	static char const * hex = "0123456789ABCDEF";
	char *foo = "00";
	foo[0] = hex[(key >> 4) & 0xF],
	foo[1] = hex[key & 0xF],
	print(foo);
}
void print_word(u16 key)
{
	print_byte((key >> 8) & 0xFF);
	print_byte( key & 0xFF);
}
void print_dword(u32 key)
{
	print_byte((key >> 24) & 0xFF);
	print_byte((key >> 16) & 0xFF);
	print_byte((key >> 8) & 0xFF);
	print_byte( key & 0xFF);
}
void print_qword(u64 key)
{
	print_byte((key >> 56) & 0xFF);
	print_byte((key >> 48) & 0xFF);
	print_byte((key >> 40) & 0xFF);
	print_byte((key >> 32) & 0xFF);
	print_byte((key >> 24) & 0xFF);
	print_byte((key >> 16) & 0xFF);
	print_byte((key >> 8) & 0xFF);
	print_byte( key & 0xFF);
}

void test(void)
{
	print("x");
}

void vgos_main(void)
{
	u8 *base = (u8*)multibootInfo->fbAddr;
	u32 width = multibootInfo->fbWidth;
	u32 height = multibootInfo->fbHeight;
	u8 bitsPerPixel = multibootInfo->fbBpp;

	if (!ega_is_active())
		return;

	ega_set_color(EGA_FG_GREEN);
	ega_clear();

	print_qword((u64)base);
	print("\n(");
	print_dword(width);
	print(", ");
	print_dword(height);
	print(" | ");
	print_dword(bitsPerPixel);
	print(")\n");

	idt_init();
	pic_init();

	pic_enable(0);
	idt_register(0x20, test);
	idt_start();

	while (1);
}
